package com.coding2themax.controller;

import java.util.Collection;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coding2themax.reptile_keeper_persistance.domain.Reptile;
import com.coding2themax.service.ReptileInventoryService;

@RestController
public class ReptileController {

	ReptileInventoryService service;

	public ReptileController(ReptileInventoryService service) {
		this.service = service;
	}

	@RequestMapping("/reptiles")
	public HttpEntity<Collection<Reptile>> getAll() {
		return new HttpEntity<Collection<Reptile>>(service.getAll());
	}

	@RequestMapping("/reptile/{id}")
	public HttpEntity<Reptile> findbyID(@PathVariable(name = "id") Long Id) {
		return new HttpEntity<Reptile>(service.findByID(Id));
	}

	@RequestMapping("/reptile")
	public HttpEntity<Reptile> findByName(@RequestParam(name = "name") String name) {
		return new HttpEntity<Reptile>(service.findByName(name));
	}

	@PostMapping("/reptiles")
	public HttpEntity<Reptile> saveReptile(@RequestBody Reptile newReptile) {
		return new HttpEntity<Reptile>(service.saveReptile(newReptile));
	}

}

package com.coding2themax.service;

import java.util.Collection;

import com.coding2themax.reptile_keeper_persistance.domain.Reptile;

public interface ReptileInventoryService {

	Collection<Reptile> getAll();

	Reptile findByID(Long id);

	Reptile findByName(String name);

	Reptile saveReptile(Reptile reptile);
}

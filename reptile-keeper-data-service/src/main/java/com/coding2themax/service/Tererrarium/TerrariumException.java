package com.coding2themax.service.Tererrarium;

public class TerrariumException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String msg;

    public TerrariumException(String message) {
        super(message);
        this.msg = message;
    }

}

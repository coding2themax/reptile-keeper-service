package com.coding2themax.service.Tererrarium;

import java.util.List;

import com.coding2themax.reptile_keeper_persistance.domain.Reptile;
import com.coding2themax.reptile_keeper_persistance.domain.Terrarium;

public interface TerrariumDataStrategy {

  List<Terrarium> getTererrariumsByRepitile(Reptile reptile);

}

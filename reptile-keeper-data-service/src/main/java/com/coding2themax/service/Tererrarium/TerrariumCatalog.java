package com.coding2themax.service.Tererrarium;

import java.util.List;

import com.coding2themax.reptile_keeper_persistance.domain.Habitat;
import com.coding2themax.reptile_keeper_persistance.domain.Reptile;
import com.coding2themax.reptile_keeper_persistance.domain.ReptileType;
import com.coding2themax.reptile_keeper_persistance.domain.Terrarium;
import com.coding2themax.service.TererrariumReptileService;

public class TerrariumCatalog {

    // Strategy pattern
    TererrariumReptileService terrariumStrategy;

    public TerrariumCatalog(TererrariumReptileService terrariumStrategy) {
        this.terrariumStrategy = terrariumStrategy;
    }

    public List<Terrarium> getTerriumsByReptile(Reptile reptile) {

        return terrariumStrategy.getTerrariumsByReptile(reptile);
    }

    // TODO: Tempalte Method pattern
    public List<Terrarium> getTererrariumsByLengthAndType(Double length, Habitat habitat, ReptileType reptileType) {
        // TerrariumDimension td = calculateTerrariumDimension(length, habitat,
        // reptileType);
        // return terrariumStrategy.getTererrariumsByDimension(td);
        return null;
    }

    public void addTerriums(Terrarium t) throws TerrariumException {
        terrariumStrategy.save(t);
    }

    public void assignReptileToTarrium(Reptile reptile, Terrarium terrarium) throws TerrariumException {
        terrariumStrategy.assignTerrariumByRepitile(terrarium, reptile);
    }

    // public void TerrariumDimension calculateTerrariumDimension(Double length,
    // Habitat habitat, ReptileType reptileType);

}
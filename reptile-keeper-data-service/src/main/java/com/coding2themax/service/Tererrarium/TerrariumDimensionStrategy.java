package com.coding2themax.service.Tererrarium;

import com.coding2themax.reptile_keeper_persistance.domain.TerrariumDimension;

public interface TerrariumDimensionStrategy {

    double getLength(TerrariumDimension terrariumDimension);

    double getWitdh(TerrariumDimension terrariumDimension);

}
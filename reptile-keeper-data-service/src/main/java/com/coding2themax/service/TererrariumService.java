package com.coding2themax.service;

import java.util.List;

import com.coding2themax.reptile_keeper_persistance.domain.Terrarium;

public interface TererrariumService {
    public List<Terrarium> getAllTerrariums();

    public Terrarium getTarrium(Terrarium t);

    public Terrarium getTerrariumByID(String ID);

    public Terrarium getTerrariumByManufacture(String manufacture);

    public void save(Terrarium t);
}

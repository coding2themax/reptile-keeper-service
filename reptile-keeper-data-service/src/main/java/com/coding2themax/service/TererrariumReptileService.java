package com.coding2themax.service;

import java.util.List;

import com.coding2themax.reptile_keeper_persistance.domain.Reptile;
import com.coding2themax.reptile_keeper_persistance.domain.Terrarium;

public interface TererrariumReptileService {

    public List<Terrarium> getTerrariumsByReptile(Reptile reptile);

    public List<Reptile> getReptilesByTerrarium(Terrarium terrarium);

    public List<Terrarium> getTerrariumsByRepitile(Reptile reptile);

    public void saveReptileByTerrarium(Reptile reptile, Terrarium terrarium);

    public void saveReptileByTerrariums(Reptile reptile, List<Terrarium> terrariums);

    public void save(Terrarium t);

    public void assignTerrariumByRepitile(Terrarium terrarium, Reptile reptile);

}
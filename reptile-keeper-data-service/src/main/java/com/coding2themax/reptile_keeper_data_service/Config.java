package com.coding2themax.reptile_keeper_data_service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories(basePackages = "com.coding2themax.reptile_keeper_persistance.repository")
@ComponentScan(basePackages = "com.coding2themax.reptile_keeper_persistance")
public class Config {

}

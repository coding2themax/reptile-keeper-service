package com.coding2themax.reptile_keeper_persistance.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.coding2themax.reptile_keeper_persistance.domain.Reptile;

public interface ReptileRepo extends ReactiveCrudRepository<Reptile, Long> {

}

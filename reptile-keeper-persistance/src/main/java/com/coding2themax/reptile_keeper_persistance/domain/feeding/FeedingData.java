package com.coding2themax.reptile_keeper_persistance.domain.feeding;

import java.util.ArrayList;
import java.util.List;

public class FeedingData implements FeedingSubject {

	private String reptileName;
	private String foodName;
	private double feedAmount;

	List<FeedingObserver> observers;

	public FeedingData() {
		observers = new ArrayList<FeedingObserver>();

	}

	@Override
	public void registerObserver(FeedingObserver o) {
		observers.add(o);

	}

	@Override
	public void removeObserver(FeedingObserver o) {

		observers.remove(o);

	}

	@Override
	public void notifiyObserver() {
		observers.forEach((o) -> {
			o.update(reptileName, foodName, feedAmount);
		});

	}

	public void feed(String reptileName, String foodName, double feedAmount) {
		this.feedAmount = feedAmount;
		this.foodName = foodName;
		this.reptileName = reptileName;
		feeding();
	}

	private void feeding() {
		notifiyObserver();
	}

	public String getReptileName() {
		return reptileName;
	}

	public String getFoodName() {
		return foodName;
	}

	public double getFeedAmount() {
		return feedAmount;
	}

}

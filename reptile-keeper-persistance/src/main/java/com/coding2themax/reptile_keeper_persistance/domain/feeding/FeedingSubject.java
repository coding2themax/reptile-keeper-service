package com.coding2themax.reptile_keeper_persistance.domain.feeding;

public interface FeedingSubject {
	public void registerObserver(FeedingObserver o);

	public void removeObserver(FeedingObserver o);

	public void notifiyObserver();

}

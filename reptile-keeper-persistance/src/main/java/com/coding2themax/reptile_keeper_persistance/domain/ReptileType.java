package com.coding2themax.reptile_keeper_persistance.domain;

public enum ReptileType {
    LIZARD,
    SNAKE,
    AQUATIC,
    TURTLES
}

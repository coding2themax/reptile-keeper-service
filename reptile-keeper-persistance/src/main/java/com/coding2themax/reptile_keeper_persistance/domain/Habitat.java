package com.coding2themax.reptile_keeper_persistance.domain;

public enum Habitat {
    AQUATIC,
    ARBOREAL,
    FOSSORIAL,
    SCANSORIAL,
    RIPARIAN,
    TERRESTRIAL
}

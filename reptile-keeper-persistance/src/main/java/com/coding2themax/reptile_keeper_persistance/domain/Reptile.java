package com.coding2themax.reptile_keeper_persistance.domain;

import java.io.Serializable;

public class Reptile implements Serializable {

	private static final long serialVersionUID = 1216923177107497693L;

	private Long Id;
	private String commonName;
	private String genus;
	private String species;
	private ReptileType reptileType;
	private Habitat habitat;
	private ReptileCategory reptileCategory;

	public Reptile() {
	}

	public Reptile(String commonName, String genus, String species, ReptileCategory reptileCategory) {
		this.commonName = commonName;
		this.genus = genus;
		this.species = species;
		this.reptileCategory = reptileCategory;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public Long getID() {
		return Id;
	}

	public void setID(Long Id) {
		this.Id = Id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public ReptileCategory getReptileCategory() {
		return reptileCategory;
	}

	public void setReptileCategory(ReptileCategory reptileCategory) {
		this.reptileCategory = reptileCategory;
	}

	public String getCommonName() {
		return commonName;
	}

	public String getGenus() {
		return genus;
	}

	public String getSpecies() {
		return species;
	}

	public ReptileType getReptileType() {
		return reptileType;
	}

	public void setReptileType(ReptileType reptileType) {
		this.reptileType = reptileType;
	}

	public Habitat getHabitat() {
		return habitat;
	}

	public void setHabitat(Habitat habitat) {
		this.habitat = habitat;
	}

}

package com.coding2themax.reptile_keeper_persistance.domain;

public class Terrarium {
    private String ID;
    private String partNumber;
    private String manufacture;
    private String descriptionString;
    private String ASIN;
    private TerrariumDimension terrariumDimension;

    public Terrarium() {
    }

    public Terrarium(String iD, String partNumber, String manufacture, String descriptionString, String aSIN) {
        ID = iD;
        this.partNumber = partNumber;
        this.manufacture = manufacture;
        this.descriptionString = descriptionString;
        ASIN = aSIN;
    }

    public String getID() {
        return ID;
    }

    public void setID(String iD) {
        ID = iD;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getDescriptionString() {
        return descriptionString;
    }

    public void setDescriptionString(String descriptionString) {
        this.descriptionString = descriptionString;
    }

    public String getASIN() {
        return ASIN;
    }

    public void setASIN(String aSIN) {
        ASIN = aSIN;
    }

    public TerrariumDimension getTerrariumDimension() {
        return terrariumDimension;
    }

    public void setTerrariumDimension(TerrariumDimension terrariumDimension) {
        this.terrariumDimension = terrariumDimension;
    }

}
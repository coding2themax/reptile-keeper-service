package com.coding2themax.reptile_keeper_persistance.domain.feeding;

public class FoodConsumptionDisplay implements FeedingObserver, DisplayElement {

	private String reptileName;
	private String foodName;
	private double feedAmount;

	private FeedingSubject subject;

	public FoodConsumptionDisplay(FeedingSubject s) {
		this.subject = s;
		subject.registerObserver(this);
	}

	@Override
	public void update(String reptileName, String foodName, double feedAmount) {

		this.reptileName = reptileName;
		this.feedAmount = feedAmount;
		this.foodName = foodName;
		display();
	}

	@Override
	public void display() {
		System.out.println("feed " + reptileName + " " + feedAmount + " " + foodName);
	}

}

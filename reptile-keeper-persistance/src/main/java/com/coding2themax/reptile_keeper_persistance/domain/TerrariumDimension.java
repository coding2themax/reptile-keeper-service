package com.coding2themax.reptile_keeper_persistance.domain;

public class TerrariumDimension {

    private Long ID;
    private Double height;
    private Double witdh;
    private Double length;

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWitdh() {
        return witdh;
    }

    public void setWitdh(Double witdh) {
        this.witdh = witdh;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long iD) {
        ID = iD;
    }

}

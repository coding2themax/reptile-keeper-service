package com.coding2themax.reptile_keeper_persistance.domain.feeding;

public interface FeedingObserver {

	public void update(String reptileName, String foodName, double feedAmount);

}

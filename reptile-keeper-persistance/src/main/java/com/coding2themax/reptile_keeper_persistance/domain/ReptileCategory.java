package com.coding2themax.reptile_keeper_persistance.domain;

public class ReptileCategory {

    private Long ID;
    private String name;

    public ReptileCategory() {
    }

    public ReptileCategory(Long iD, String name) {
        ID = iD;
        this.name = name;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long iD) {
        ID = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

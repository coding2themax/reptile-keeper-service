package com.coding2themax.reptile_keeper_persistance.domain.feeding;

public class FoodInventoryDisplay implements FeedingObserver, DisplayElement {

	private double initialAmount = 100.0;

	private double feedAmount;

	private FeedingSubject subject;

	public FoodInventoryDisplay(FeedingData fd) {
		subject = fd;
		subject.registerObserver(this);
	}

	@Override
	public void display() {
		System.out.println("Feed amount left " + (initialAmount - feedAmount));
	}

	@Override
	public void update(String reptileName, String foodName, double feedAmount) {
		this.feedAmount = feedAmount;
		display();
	}

}

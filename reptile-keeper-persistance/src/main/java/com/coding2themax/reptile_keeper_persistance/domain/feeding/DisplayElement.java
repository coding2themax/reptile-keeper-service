package com.coding2themax.reptile_keeper_persistance.domain.feeding;

public interface DisplayElement {

	public void display();

}
